# Ubermag Binder Demo

[![Binder](https://notebooks.mpcdf.mpg.de/binder/badge_logo.svg)](https://notebooks.mpcdf.mpg.de/binder/v2/git/https%3A%2F%2Fgitlab.mpcdf.mpg.de%2Fubermag%2Fubermag_demo/HEAD?labpath=demo.ipynb) 


## About

This repository provides demonstration of how to use bunder with Ubermag. All notebooks hosted in this repository can be run in the cloud (see
next section on Binder), and thus the results reproduced by anybody.


## Binder

Jupyter notebooks hosted in this repository can be executed and modified in the
cloud via Binder. This does not require you to have anything installed and no
files will be created on your machine. To access Binder, click on the Binder
badge.

# Notebooks to learn `ubermag`

The [demo](demo.ipynb) notebooks shows a concise example - dynamics of a
magnetic vortex - to demonstrate the main features of Ubermag.

## Getting started

If you are new to `Ubermag` this is the correct place to start learning it.
This set of tutorials will introduce all basic concepts of `Ubermag`
and its subpackages.

## Examples

The notebooks shown in this section are all tailored to physical questions and
demonstrate how to solve them using ``Ubermag``. The tutorials try to be concise
and only cover one use case. They do not explain every little detail. For a more
technical documentation of specific features please refer to the reference and
api show on the [website](https://ubermag.github.io).

